package route

import (
	"github.com/gin-gonic/gin"
	"login.sa.ru/controller"
	"login.sa.ru/route/middleware"
)

func RegisterRoute(server *gin.Engine)  {
	server.Use(middleware.CORS())
	server.Use(middleware.HandleErrors()).GET("/check", controller.RegisterCheck)
	server.Use(middleware.HandleErrors()).GET("/contact", controller.RegisterContact)
	server.Use(middleware.HandleErrors()).GET("/code", controller.RegisterCode)
	server.Use(middleware.HandleErrors()).GET("/password", controller.RegisterPassword)
}
