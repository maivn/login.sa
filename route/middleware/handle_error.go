package middleware

import (
	"github.com/gin-gonic/gin"

	"net/http"
	e "login.sa.ru/shared/errors"
)

type Err struct {
	Code int
	Msg  string
}

func HandleErrors() gin.HandlerFunc {
	return func(context *gin.Context) {
		defer func() {
			if err := recover(); err != nil {
				if es, ok := err.(*e.Errors); ok {
					context.AbortWithStatusJSON(http.StatusInternalServerError, es)
				} else if e, ok := err.(e.Error); ok {
					context.AbortWithStatusJSON(http.StatusInternalServerError, e)
				} else {
					context.AbortWithStatusJSON(http.StatusInternalServerError, err)
				}
			}
		}()
		context.Next()
	}
}
