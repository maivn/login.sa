package user

import (
	"github.com/jinzhu/gorm"
	e "login.sa.ru/shared/errors"
	"login.sa.ru/shared/database"
	"strings"
	"github.com/gin-gonic/gin"
	"login.sa.ru/shared/validation"
)

type User struct {
	gorm.Model
	Name            string `form:"name" json:"name"`
	Password        string `form:"password" json:"password" validate:"required"`
	PasswordConfirm string `gorm:"-" form:"password_confirm" json:"password_confirm" validate:"required"`
}

func (user *User) Fill(context *gin.Context) {
	if err := context.Bind(user); err != nil {
		e.Errs.Add(e.Error{Code: e.ErrorBinding}).Exist()
	}
}

func (user *User) Validate(fields ...string) {
	for _, field := range fields {
		err := validation.Validate.StructPartial(user, field)
		if err != nil {
			e.Errs.Add(e.Error{Code: e.ErrorValidation, SubCode: strings.ToLower(field), Msg: err.Error()})
		}
	}
	e.Errs.Exist()
}


func (u *User) CheckPasswords()  {
	if u.Password != u.PasswordConfirm {
		panic(e.Errs.Add(e.Error{Code:e.ErrorConfirmPassword}))
	}
}

func GetByContactPassword(Type string, Value string, Password string) *User {
	var user User
	return &user
}

func Save(user *User) {
	if database.Sql.Save(user).Error != nil {
		panic(e.Error{Code: e.ErrorUserSave})
	}
}
