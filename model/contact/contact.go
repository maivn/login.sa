package contact

import (
	"github.com/jinzhu/gorm"

	e "login.sa.ru/shared/errors"
	"login.sa.ru/shared/database"
	"github.com/gin-gonic/gin"
	"login.sa.ru/shared/validation"
	"strings"
)

type Contact struct {
	gorm.Model
	Type    string `form:"type" json:"type" validate:"required,eq=email|eq=phone"`
	Value   string `form:"value" json:"value" validate:"required"`
	UserId  uint
	Code    string `form:"code" json:"code" validate:"required" gorm:"-"`
	Confirm bool   `gorm:"-"`
}

func (contact *Contact) Fill(context *gin.Context) {
	if err := context.Bind(contact); err != nil {
		e.Errs.Add(e.Error{Code: e.ErrorBinding}).Exist()
	}
}

func (contact *Contact) Validate(fields ...string) {
	for _, field := range fields {
		err := validation.Validate.StructPartial(contact, field)
		if err != nil {
			e.Errs.Add(e.Error{Code: e.ErrorValidation, SubCode: strings.ToLower(field), Msg: err.Error()})
		}
	}
	e.Errs.Exist()
}

func (contact *Contact) ValidateValueByType() {
	contact.Validate("Type", "Value")
	err := validation.Validate.Var(contact.Value, contact.Type)
	if err != nil {
		e.Errs.Add(e.Error{Code: e.ErrorValidation, SubCode: contact.Type, Msg: err.Error()}).Exist()
	}
}

func (contact *Contact)ExistByTypeValue(){
	if GetByTypeValue(contact.Type, contact.Value) != nil {
		e.Errs.Add(e.Error{Code: e.ErrorContactExist}).Exist()
	}
}

func GetByID(id uint) *Contact {
	var contact Contact
	contact.Type = "TestType"
	contact.Value = "TestValue"
	return &contact
}

func GetByTypeValue(Type string, Value string) *Contact {
	var contact Contact
	if database.Sql.Where("type = ?", Type).
		Where("value = ?", Value).First(&contact).RecordNotFound() {
		return nil
	}
	return &contact
}

func Save(contact *Contact) {
	if database.Sql.Save(contact).Error != nil {
		e.Errs.Add(e.Error{Code: e.ErrorContactSave}).Exist()
	}
}

func (c *Contact) CheckCode(code string) {
	if c.Code != code {
		e.Errs.Add(e.Error{Code: e.ErrorInvalidCode}).Exist()
	}
}

func (c *Contact) IsConfirm() {
	if !c.Confirm {
		e.Errs.Add(e.Error{Code: e.ErrorContactNotConfirm}).Exist()
	}
}
