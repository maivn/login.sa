package model

import (
	"strings"

	"github.com/gin-gonic/gin"

	e "login.sa.ru/shared/errors"
	v "login.sa.ru/shared/validation"
	"fmt"
)

type Entity struct {
}

func (entity *Entity) Fill(context *gin.Context) {
	if err := context.Bind(&entity); err != nil {
		e.Errs.Add(e.Error{Code: e.ErrorBinding}).Exist()
	}
}

func (entity *Entity) ValidateFields(fields ...string) {
	for _, field := range fields {
		fmt.Println(entity, field)
		err := v.Validate.StructPartial(entity, field)
		fmt.Println(entity, field, err)
		if err != nil {
			e.Errs.Add(e.Error{Code: e.ErrorValidation, SubCode: strings.ToLower(field), Msg: err.Error()})
		}
	}
	e.Errs.Exist()
}

func (entity *Entity) ValidateField(field string, tag string) {
	err := v.Validate.Var(field, tag)
	if err != nil {
		e.Errs.Add(e.Error{Code: e.ErrorValidation, SubCode: strings.ToLower(field), Msg: err.Error()}).Exist()
	}
}
