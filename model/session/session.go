package session

import (

	"time"
)

type Session struct {
	UserId uint
	Expire time.Time
}
