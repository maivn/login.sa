package controller

import (
	"github.com/gin-gonic/gin"
	"login.sa.ru/model/contact"
	"fmt"
	"login.sa.ru/shared/utils"
	"login.sa.ru/shared/redis"
	"login.sa.ru/shared/json"
	"time"
	"login.sa.ru/model/user"

	"os"
	"login.sa.ru/shared/response"
	"login.sa.ru/shared/cookie"
)

func RegisterCheck(context *gin.Context) {
	cid := cookie.GetCookie(context, "cid")
	fmt.Println(cid)
}

func RegisterContact(context *gin.Context) {
	var Contact contact.Contact
	Contact.Fill(context)
	Contact.ValidateValueByType()
	Contact.ExistByTypeValue()

	cid := utils.GenerateUuid()
	Contact.Code = utils.GenerateDigitalCode(6)

	// send code
	fmt.Println("Code", Contact.Code)
	//*********

	redis.SetValue(cid, json.JsonEncode(&Contact), 5*60*time.Second)
	context.SetCookie("cid", cid, 10*60, "", os.Getenv("APP_DOMAIN"), false, false)
	context.JSON(200, response.Response{cid})
}

func RegisterCode(context *gin.Context) {
	var Contact contact.Contact
	Contact.Fill(context)
	Contact.Validate("Type", "Value", "Code")

	cid := cookie.GetCookie(context, "cid")
	data := redis.GetValue(cid)

	var storageContact contact.Contact
	json.JsonDecode(data, &storageContact)

	storageContact.CheckCode(Contact.Code)
	storageContact.Confirm = true

	redis.SetValue(cid, json.JsonEncode(&storageContact), 5*60*time.Second)

	context.SetCookie("cid", cid, 10*60, "", os.Getenv("APP_DOMAIN"), false, false)
	context.JSON(200, response.Response{cid})
}

func RegisterPassword(context *gin.Context) {
	var User user.User
	User.Fill(context)
	User.Validate("Password", "PasswordConfirm")
	User.CheckPasswords()

	cid := cookie.GetCookie(context, "cid")
	data := redis.GetValue(cid)

	var storageContact contact.Contact
	json.JsonDecode(data, &storageContact)

	storageContact.ExistByTypeValue()
	storageContact.IsConfirm()

	user.Save(&User)
	storageContact.UserId = User.ID
	contact.Save(&storageContact)
	context.JSON(200, User.ID)
}
