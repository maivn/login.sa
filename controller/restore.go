package controller

import (
	"github.com/gin-gonic/gin"
	"login.sa.ru/shared/binding"
	"login.sa.ru/model/contact"
	"fmt"
	"os"
	"login.sa.ru/shared/validation"
	"login.sa.ru/shared/utils"
	"login.sa.ru/shared/redis"
	"login.sa.ru/shared/json"
	"time"
	"login.sa.ru/model/user"
)

func RestoreContact(context *gin.Context) {
	var Contact contact.Contact
	binding.Fill(context, &Contact)
	validation.ValidationPartial(&Contact, "Type", "Value")
	if contact.GetByTypeValue(Contact.Type, Contact.Value) == nil {
		panic("not exist contact")
	}
	cid := utils.GenerateUuid()
	Contact.Code = utils.GenerateDigitalCode(6)

	// send code
	fmt.Println("Code", Contact.Code)
	//*********

	if _, err := redis.Client.Set(cid, json.JsonEncode(&Contact), 5*60*time.Second).Result();
	err != nil {
		panic("redis error")
	}

	context.SetCookie("cid", cid, 10*60, "", os.Getenv("APP_DOMAIN"), false, false)
	context.JSON(200, "")
}

func RestoreCode(context *gin.Context) {
	var Contact contact.Contact
	binding.Fill(context, &Contact)
	validation.ValidationPartial(&Contact, "Type", "Value", "Code")

	cid, err := context.Cookie("cid")
	if err != nil {
		panic("error get cid")
	}

	value, err := redis.Client.Get(cid).Result();
	if err != nil {
		panic("redis error")
	}

	var storageContact contact.Contact
	json.JsonDecode(value, storageContact)

	if Contact.Code != storageContact.Code {
		panic("invalid code")
	}

	Contact.Confirm = true

	if _, err := redis.Client.Set(cid, json.JsonEncode(&Contact), 5*60*time.Second).Result();
	err != nil {
		panic("redis error")
	}

	context.SetCookie("cid", cid, 10*60, "", os.Getenv("APP_DOMAIN"), false, false)
	context.JSON(200, "")
}

func RestorePassword(context *gin.Context) {
	var User user.User
	binding.Fill(context, &User)
	validation.ValidationPartial(&User, "Password", "PasswordConfirm")

	if User.Password != User.PasswordConfirm {
		panic("Password != PasswordConfirm")
	}

	cid, err := context.Cookie("cid")
	if err != nil {
		panic("error get cid")
	}

	value, err := redis.Client.Get(cid).Result();
	if err != nil {
		panic("redis error")
	}

	var storageContact contact.Contact
	json.JsonDecode(value, storageContact)

	if !storageContact.Confirm {
		panic("contact not confirm")
	}

	user.Save(&User)
	contact.Save(&storageContact)
	context.JSON(200, "")
}


