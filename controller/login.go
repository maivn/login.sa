package controller

import (
	"github.com/gin-gonic/gin"
	"login.sa.ru/model/contact"
	"login.sa.ru/shared/validation"
	"login.sa.ru/model/user"
	"login.sa.ru/model/session"
	"login.sa.ru/shared/utils"
	"login.sa.ru/shared/redis"
	"os"
)

func Login(context *gin.Context)  {
	var Contact contact.Contact
	validation.ValidationPartial(&Contact, "Type", "Value")

	var User user.User
	validation.ValidationPartial(&User, "Password")

	findUser := user.GetByContactPassword(Contact.Type, Contact.Value, User.Password)
	if findUser == nil {
		panic("invalid login/password")
	}

	var Session session.Session
	Session.UserId = findUser.ID
	ssid := utils.GenerateUuid()

	redis.Client.Set(ssid, Session, 10*60)
	context.SetCookie("ssid", ssid, 10*60, "", os.Getenv("APP_DOMAIN"), false, false)
}

func Logout(context *gin.Context)  {
	context.SetCookie("ssid", "", 1, "", os.Getenv("APP_DOMAIN"), false, false)
}