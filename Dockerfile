FROM golang:1.8

WORKDIR /go/src/login.sa.ru
COPY . .

RUN go get "github.com/gin-gonic/gin"
RUN go get "github.com/go-redis/redis"
RUN go get "github.com/jinzhu/gorm"
RUN go get "github.com/joho/godotenv"
RUN go get "github.com/lib/pq"
RUN go get "github.com/satori/go.uuid"
RUN go get "gopkg.in/go-playground/validator.v9"

RUN go install

ENV PORT 8000
EXPOSE 8000

CMD ["login.sa.ru"]