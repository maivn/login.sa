package utils

import (
	"io"
	"crypto/rand"
	"github.com/satori/go.uuid"
	e "login.sa.ru/shared/errors"
)

func GenerateDigitalCode(lenght int) string {
	digits := [...]byte{'1', '2', '3', '4', '5', '6', '7', '8', '9', '0'}
	code := make([]byte, lenght)
	n, err := io.ReadAtLeast(rand.Reader, code, lenght)
	if n != lenght || err != nil {
		e.Errs.Add(e.Error{Code: e.ErrorGenerateCode}).Exist()

		panic(err)
	}
	for i := 0; i < len(code); i++ {
		code[i] = digits[int(code[i])%len(digits)]
	}
	return string(code)
}

func GenerateUuid() string {
	u, err := uuid.NewV4()
	if err != nil {
		e.Errs.Add(e.Error{Code: e.ErrorGenerateUuid}).Exist()
	}
	return u.String()
}