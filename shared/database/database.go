package database

import (
	"fmt"
	"os"

	"github.com/jinzhu/gorm"

	_"github.com/lib/pq"
)

var Sql *gorm.DB

func Init() {
	var err error
	Sql, err = gorm.Open(os.Getenv("DB_DRIVER"), fmt.Sprintf("user=%s password=%s dbname=%s host=%s port=%s sslmode=disable",
		os.Getenv("DB_USER"), os.Getenv("DB_PASSWORD"), os.Getenv("DB_NAME"), os.Getenv("DB_HOST"), os.Getenv("DB_PORT")))
	if err != nil {
		fmt.Println(err.Error())
		panic("Failed to connect database")
	}
}

