package json

import "encoding/json"

func JsonEncode(i interface{}) string {
	bytes, err := json.Marshal(i)
	if err != nil {
		panic(err)
	}
	return string(bytes)
}

func JsonDecode(str string, i interface{}) {
	err := json.Unmarshal([]byte(str), i)
	if err != nil {
		panic(err)
	}
}
