package validation

import (
	"gopkg.in/go-playground/validator.v9"
	e "login.sa.ru/shared/errors"
	"strings"
	"fmt"
)

var Validate *validator.Validate

func init() {
	Validate = validator.New()
	Validate.RegisterValidation("phone", phoneValidFunc)
}

func ValidationPartial(i interface{}, fields ...string) {
	for _, field := range fields {
		fmt.Println(i)
		err := Validate.StructPartial(i, field)
		if err != nil {
			e.Errs.Add(e.Error{Code: e.ErrorValidation, SubCode: strings.ToLower(field), Msg: err.Error()})
		}
	}
	e.Errs.Exist()
}

func phoneValidFunc(fl validator.FieldLevel) bool {
	return false
}

func Field(field interface{}, tag string)  {
	err := Validate.Var(field, tag)
	if err != nil {
		e.Errs.Add(e.Error{Code: e.ErrorValidation, SubCode: tag, Msg: err.Error()}).Exist()
	}
}