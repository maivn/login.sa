package errors

import "fmt"

type Error struct {
	Code          int		`json:"code"`
	SubCode       string	`json:"sub_code"`
	Msg           string	`json:"msg"`
}

type Errors struct {
	Errors []Error	`json:"errors"`
}
var Errs Errors

type RequestParam struct {
	Key   string
	Value string
}

func (es *Errors) Exist() {
	fmt.Println(es)
	if len(es.Errors) != 0 {
		panic(es.Remove())
	}
}

func (es *Errors) Add(e Error) *Errors {
	es.Errors = append(es.Errors, e.fill())
	return es
}

func (es *Errors) Remove() *Errors {
	var errs = *es
	es.Errors = es.Errors[:0]
	return &errs
}

func (e *Error) fill() Error {
	fmt.Println(e.Msg)
	if e.Msg == "" {
		fmt.Println(e.Msg)
		fmt.Println(GetMessageByCode(e.Code))
		e.Msg = GetMessageByCode(e.Code)
	}
	return *e
}

func GetMessageByCode(code int) string {
	if msg, has := errorText[code]; has {
		return msg
	}
	return "unknow error"
}

const (
	ErrorValidation        = 4000
	ErrorValidationExcept  = 4001
	ErrorValidationPartial = 4002

	ErrorContactNotFound   = 4010
	ErrorContactExist      = 4011
	ErrorContactNotConfirm = 4012
	ErrorContactSave       = 4013

	ErrorUserSave = 4113

	ErrorInvalidCode = 4020

	ErrorEasyPassword    = 4030
	ErrorConfirmPassword = 4031

	ErrorGetCookie = 4050

	ErrorBinding       = 5000
	ErrorRedis         = 5010
	ErrorRedisNotFound = 5011

	ErrorGenerateUuid = 5020
	ErrorGenerateCode = 5030
)

var errorText = map[int]string{
	ErrorValidation:        "Error validation",
	ErrorValidationExcept:  "Error validation except",
	ErrorValidationPartial: "Error validation partial",

	ErrorContactNotFound:   "Error contact not found",
	ErrorContactExist:      "Error contact exist",
	ErrorContactNotConfirm: "Error contact not confirm",
	ErrorContactSave:       "Error contact save",


	ErrorUserSave: "Error contact save",

	ErrorEasyPassword:    "Error easy password",
	ErrorConfirmPassword: "Error confirm password",

	ErrorInvalidCode: "Error invalid code",

	ErrorGetCookie: "Error get cookie",

	ErrorBinding:       "Error binding",
	ErrorRedis:         "Error redis",
	ErrorRedisNotFound: "Error redis not found",

	ErrorGenerateUuid: "Error generate uuid",
	ErrorGenerateCode: "Error generate code",
}
