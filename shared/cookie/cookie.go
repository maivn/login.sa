package cookie

import (
	"github.com/gin-gonic/gin"
	e"login.sa.ru/shared/errors"
)

func SetCookie() {
}

func GetCookie(context *gin.Context, key string) string {
	value, err := context.Cookie(key)
	if err != nil {
		panic(e.Errs.Add(e.Error{Code: e.ErrorGetCookie}))
	}
	return value
}
