package redis

import (
	"github.com/go-redis/redis"
	"os"
	"time"
	e "login.sa.ru/shared/errors"

)

var Client *redis.Client

func Init() {
	Client = redis.NewClient(&redis.Options{
		Addr:     os.Getenv("REDIS_HOST") + ":" + os.Getenv("REDIS_PORT"),
		Password: os.Getenv("REDIS_PASSWORD"), // no password set
		DB:       0,  // use default DB
	})
	_, err := Client.Ping().Result()
	if err != nil {
		panic(err.Error())
	}
}

func SetValue(key string, value string, duration time.Duration)  {
	if _, err := Client.Set(key, value, duration).Result(); err != nil {
		panic(e.Error{Code: e.ErrorRedis})
	}
}

func GetValue(key string) string {
	value, err := Client.Get(key).Result();
	if err != nil {
		panic(e.Error{Code: e.ErrorRedisNotFound})
	}
	return value
}
