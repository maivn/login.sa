package binding

import (
	"github.com/gin-gonic/gin"
	e "login.sa.ru/shared/errors"
)

func Fill(context *gin.Context, i interface{}) {
	if err := context.Bind(i); err != nil {
		e.Errs.Add(e.Error{Code:e.ErrorBinding}).Exist()
	}
}
