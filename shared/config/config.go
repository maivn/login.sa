package config

import (
	"github.com/joho/godotenv"
)

func Load(files ...string)  {
	err := godotenv.Load(files...)
	if err != nil {
		panic(err.Error())
	}
}
