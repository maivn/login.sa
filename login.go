package main

import (
	"github.com/gin-gonic/gin"
	"login.sa.ru/route"
	"login.sa.ru/shared/config"
	"login.sa.ru/shared/database"
	"login.sa.ru/shared/redis"
	"login.sa.ru/model/user"
	"login.sa.ru/model/contact"
	"os"
)

func init() {
	config.Load("./dev.env")
	database.Init()
	redis.Init()
	database.Sql.AutoMigrate(user.User{}, contact.Contact{})
}

func main() {
	server := gin.Default()
	route.RegisterRoute(server)
	server.Run(":" + os.Getenv("APP_PORT"))
}
